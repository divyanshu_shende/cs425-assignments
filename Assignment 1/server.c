// Invocation: ./server port-number
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h> //To check for file in directory!
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <signal.h>

#define BUF_LEN 256
int SERVER_PORT;
char buf[BUF_LEN];
char welcome_message[]="Welcome to the file service!\n";
char welcome_sent[]="Welcome message sent to client!\n";
char regret[]="No such file exists!\n";
char error_mess[]="Error!\n";
char request[]="Client requested for a file.\n";

int main(int argc, char *argv[])
{
  char buf[BUF_LEN];
  int serv_socket, conn_socket, alive=1;
  struct sockaddr_in serverAddr;
  // Ignore signal to kill server if client quits.
  signal(SIGPIPE,SIG_IGN);

    if(argc!=2)
    {
      fwrite(error_mess, sizeof(char), strlen(error_mess), stdout);
      // size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream );
      exit(1);
    }
    else
      SERVER_PORT = atoi(argv[1]); //Read port number from argv and convert to int

    // Initialize buffer
    memset(buf, '\0', BUF_LEN);

    // Creating server socket
    serv_socket=socket(PF_INET, SOCK_STREAM, 0);

    // Configuring address structure for the bind()

    // Server socket's address family
    serverAddr.sin_family = AF_INET;

    // Port of which server socket is listening
    serverAddr.sin_port = htons(SERVER_PORT);

    // inet_addr converts string to IP address in IPv4 notation
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    //Binding the server socket to the port
    bind(serv_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    // int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);


    while(1)
    {
        // Look at the ending of this loop for conection termination.
        // If the client has exited, close that socket to create new connection.
        if(alive <= 0)
        {
          //close client socket!
          shutdown(conn_socket, 2);
        }

        //Listening with atmost 5 clients in queue
        listen(serv_socket, 5);
        // int listen(int sockfd, int backlog);


        // Accept connections from client
        int addr_len = sizeof(serverAddr);
        conn_socket = accept(serv_socket, (struct sockaddr *)&serverAddr, &addr_len);
        // int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);

        // If connection fails, break out of loop
        if(conn_socket < 0)
            exit(1);

        // Welcome client
        strcpy(buf, welcome_message);

        int wml = send(conn_socket, buf, BUF_LEN, 0);
        fwrite(welcome_sent, sizeof(char), strlen(welcome_sent), stdout);
        // ssize_t send(int sockfd, const void *buf, size_t len, int flags);

        //Beginning service
        while(1)
        {       
          // Receive filename from client
          int fname_len = recv(conn_socket, buf, BUF_LEN, 0);
          // printf("Request for filename %s received from client %d\n", buf, conn_socket);
          if(fname_len < 0)
          {
            // Send regret message in case on invalid filename!
            strcpy(buf, regret);
          }

          // Get file name from buffer
          char fname[BUF_LEN];
          strcpy(fname, buf);

          // Making server a little less dull
          fwrite(request, sizeof(char), strlen(request), stdout);

          // Using stat to check if file exists in current directory. 
          // stat returns 0 if file exists, -1 otherwise.
          struct stat sb;
          int n_send;
          int checkfile = stat(fname, &sb);
          int fsize = (int)(sb.st_size)*8;
          if(checkfile == -1)
          {
            n_send = 1;
          }
          
          if(checkfile == 0)
          {
              memset(buf, '\0', BUF_LEN);
              // Open file
              FILE *fp = fopen(fname, "r");
              // Size of file in bits
              long long fsize = (long long)(sb.st_size)*8;

              // Number of send required to send entire file
              n_send = fsize/BUF_LEN;
              
              if(fsize%BUF_LEN == 0)
                n_send += 0;
              else
                n_send++;

              // Send this number to client
              send(conn_socket, &n_send, sizeof(int), 0);
              int k;


              for(k=0;k<n_send;k++)
              {
                  // Need to keep track of buffer count
                  int buf_count=0;
                  char c;

                  // Keep reading and storing in buffer till there is something to read.
                  while(1)//fscanf(fp, "%c", &buf[buf_count]) == 1)
                  {
                    char c;
                    int t=fread(&c, 1, sizeof(c), fp);
                    if(t>0)
                      buf[buf_count] = c;
                    else
                      break;
                    // Increment buffer count
                    buf_count = buf_count + 1;

                    // If the buffer is full
                    if(buf_count == BUF_LEN)
                      break;
                  }
                  // Clear count
                  buf_count=0;
                  // Send part of file to client
                  send(conn_socket, buf, BUF_LEN, 0);
                  // Reset buffer
                  memset(buf, '\0', BUF_LEN);
                  // Goto next send!
              }

          }

          if(checkfile == -1)
          {
            // Tell conn_socket, we're sending just one message
            n_send = 1;
            send(conn_socket, &n_send, sizeof(int), 0);

            // Send file not found
            strcpy(buf, regret);
            send(conn_socket, buf, BUF_LEN, 0);
          }

          // alive1 is a dummy variable. If the client quits, it won't send and hence recv() returns -1.
          // This is our signal for connection termination.
          int alive1;
          alive = recv(conn_socket, &alive1, sizeof(int), 0);
          if(alive <= 0)
            break;
        }   // close the while(1) for this client
    }       // close the while(1) for listening
    return 0;
}
