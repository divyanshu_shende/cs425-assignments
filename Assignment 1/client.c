// Invocation : ./client server-IP-address port-number
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h> //To check for file in directory!
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>

#define BUF_LEN 256
int SERVER_PORT;
char buf[BUF_LEN];
char file_prompt[]="\n\nEnter file name: ";
char error_mess[]="Error!\n";
char newline[]="\n";

int main(int argc, char *argv[])
{
  int client_socket;
  struct sockaddr_in serverAddr;
  
  if(argc != 3)
  {
      fwrite(error_mess, sizeof(char), strlen(error_mess), stdout);
      // size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream );
      exit(1);
  }
  else
  {
    // Server's port is stored as char in argv[2], convert it to int
    SERVER_PORT = atoi(argv[2]);
  }

  // Initialize buffer
  memset(buf, '\0', BUF_LEN);

  // Create Client Socket
  client_socket = socket(PF_INET, SOCK_STREAM, 0);
  // int socket(int domain, int type, int protocol);

  // Details of server address and port number

  // Server socket's address family
  serverAddr.sin_family = AF_INET;
  
  // Port of which server socket is listening.
  serverAddr.sin_port = htons(SERVER_PORT);

  // argv[1] has server's IP address as string, inet_addr converts to IP address in IPv4 notation
  serverAddr.sin_addr.s_addr = inet_addr(argv[1]);


  // We are now ready to connect to server after we've configured the address structure with server's address
  int conn = connect(client_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  // int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
  
  if(conn < 0)
  {
    fwrite(error_mess, sizeof(char), strlen(error_mess), stdout);
    exit(1);
  }

  // Connection Established
  // Wait for server's welcome message to be received
  // ssize_t recv(int sockfd, void *buf, size_t len, int flags);
  int wcm = recv(client_socket, buf, BUF_LEN, 0);
  fwrite(buf, sizeof(char), strlen(buf), stdout);
  fwrite(newline, sizeof(char), strlen(newline), stdout);
  

  // start requesting
  while(1)
  {
      // Prompt user for filename.
      fwrite(file_prompt, sizeof(char), strlen(file_prompt), stdout);	
      char fname [256];
      memset(buf, '\0', 256);
      fflush(stdin);
      // Input file name character by character till you read '\n'
      int i;
      for(i=0;i<256;i++)
      {
        char c;
        fread(&c, 1, sizeof(c), stdin);
        fname[i]=c;
        if(c=='\n')
        {
          break;
        }
        fflush(stdout);
        fflush(stdin);
      }
      // Remove the '\n' read from filename
      fname[i] = '\0';

      // Filename goes to buffer
      strcpy(buf, fname);
      
      // Send filename to server
      send(client_socket, buf, BUF_LEN, 0);
      // ssize_t send(int sockfd, const void *buf, size_t len, int flags);

      // Clear buffer
      memset(buf, '\0', BUF_LEN);


      // Server first sends the number of send messages it is going to send!
      int n_send;
      recv(client_socket, &n_send, sizeof(int), 0);
      

      // Receive n_send many buffers full of file data from server and print them
      while(n_send--)
      {
          recv(client_socket, buf, BUF_LEN, 0);
          fwrite(buf, sizeof(char), strlen(buf), stdout);
      }
      int alive1=1;
      int alive = send(client_socket, &alive1, sizeof(int), 0);
  }
  return 0;
}

// size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream );
